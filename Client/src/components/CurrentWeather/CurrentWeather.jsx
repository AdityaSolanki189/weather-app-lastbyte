import React from "react";
import "./CurrentWeather.css";

const CurrentWeather = ({ weatherData }) => {

    const getDate = () => {
        const days = [
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
        ];
        const months = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December',
        ];
        const currentDate = new Date();
        const date = `${days[currentDate.getDay()]} ${currentDate.getDate()} ${months[currentDate.getMonth()]}`;
        return date;
    }

    return (
        <>  
            <div className="date">
                <h1>{getDate()}</h1>
            </div>

            <div className="current-weather">
                <div className="location">
                    <h1>{weatherData.city.name}, {weatherData.city.country}</h1>
                    
                    <h2>{Math.round(weatherData.list[0].main.temp)}°C</h2>
                </div>
                <div className="weather">
                    <img src={`icons/${weatherData.list[0].weather[0].icon}.png`} alt="weather" className="weather-icon" />
                    <h1>{weatherData.list[0].weather[0].description}</h1>
                </div>
            </div>
            <div className="title">
                <h1>Air Conditions</h1>
            </div>
            <div className="air-conditions">
                {/* Cloudiness, Humidity, Atm. Pressure, Wind Speed, Visibility, */}
                <div className="clouds">
                    
                    <h1>Clouds</h1>
                    
                    <span className="cloud-data">
                        <img src={`icons/clouds.png`} alt="clouds" />
                        <h1>{weatherData.list[0].clouds.all}%</h1>
                    </span>
                </div>

                <div className="humidity">
                    
                    <h1>Humidity</h1>

                    <span>
                        <img src={`icons/humidity.png`} alt="humidity" />
                        <h1>{weatherData.list[0].main.humidity}%</h1>
                    </span>
                </div>

                <div className="pressure">

                    <h1>Pressure</h1>

                    <span>
                        <img src={`icons/pressure.png`} alt="pressure" />
                        <h4 >{weatherData.list[0].main.pressure}hPa</h4>
                    </span>
                </div>

                <div className="wind">

                    <h1>Wind</h1>

                    <span>
                        <img src={`icons/wind.png`} alt="wind" />
                        <h4>{weatherData.list[0].wind.speed}m/s</h4>
                    </span>                    
                </div>

                <div className="visibility">

                    <h1>Visibility</h1>

                    <span>
                        <img src={`icons/visible.png`} alt="visible" />
                        <h4>{(weatherData.list[0].visibility)/1000}km</h4>
                    </span>
                </div>
            </div>
        </>
    );
};

export default CurrentWeather;
