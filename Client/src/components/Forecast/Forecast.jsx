import React from "react";
import "./Forecast.css";

const Forecast = ({ forecast }) => {
    return (
        <div>
        <h1>{forecast.name}</h1>
        <h2>{forecast.weather[0].description}</h2>
        <h3>{forecast.main.temp}</h3>
        </div>
    );
};

export default Forecast;