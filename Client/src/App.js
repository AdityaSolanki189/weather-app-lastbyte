// Import Components
import CurrentWeather from './components/CurrentWeather/CurrentWeather';

import './App.css';
import React, { useState } from 'react';
import searchIcon from "./assets/search.svg";
import {Puff} from 'react-loader-spinner';
import axios from 'axios';

// Node.js server hosted on localhost
const localhost_url = 'http://localhost:3500';

// Node.js server hosted on Render.com
const hosted_url = 'https://weather-api-cached-server.onrender.com'

function App() {

    const [searchValue, setSearchValue] = useState('');
    const [weatherInfo, setWeatherInfo] = useState(null);
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(false);
    
    const onSubmitHandler = async (e) => {
        setSearchValue("");
        setWeatherInfo(null);

        if(searchValue.length >= 3){
            e.preventDefault();
        
            setError(false);
            setLoading(true);

            await axios({
                method: "GET",
                url: `${hosted_url}/api/weather?q=${searchValue}`
            })
            .then((res) => {
                setWeatherInfo(res.data);
                setLoading(false);
            })
            .catch((err) => {
                setWeatherInfo(null);
                setSearchValue("");
                setLoading(false);
                setError(true);
            });
        }
        else{
            e.preventDefault();
            setError(true);
        }
    }

    return (
        
        <div className="App">
            <h1>Weather App 🌤️</h1>
        
            <div className="search">
                <form onSubmit={onSubmitHandler}>
                    <input
                        type="text"
                        placeholder="Search for a City"
                        value={searchValue}
                        onChange={(e) => setSearchValue(e.target.value)}
                    />
                    <img src={searchIcon} alt="search" style={{ cursor: 'pointer'}} onClick={onSubmitHandler}/>
                </form>
            </div>

            {loading && (
                <div className="loading">
                    <Puff className="loader"
                        height="100"
                        width="100"
                        radius={1}
                        color="#2B3467"
                        ariaLabel="puff-loading"
                        visible={true}
                    />
                </div>
            )}

            {error && (
                <div className="error-message">
                    <p>⚠️ City Not Found ⚠️</p>
                </div>
            )}
            
            {weatherInfo && (
                <CurrentWeather weatherData = {weatherInfo}/>
            )}
        </div>
        
    );
}

export default App;
