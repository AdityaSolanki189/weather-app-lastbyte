const dotEnv = require('dotenv');
dotEnv.config();
const express = require('express');
const router = express.Router();
const axios = require('axios');

const cache = require('../middleware/routeCache');

const OPEN_WEATHER_API_URL = "https://api.openweathermap.org/data/2.5/forecast";

// GET /weather?q=Location -> Location is the query parameters
router.get("/weather", cache(400), async function(req, res){
    const location = req.query.q;

    try{
        const response = await axios({
            method: 'GET',
            url: `${OPEN_WEATHER_API_URL}`,
            params: {q: location, units: 'metric', appid: `${process.env.OPEN_WEATHER_API_KEY}`},
        });
    
        const data = await response.data;
        // console.log(data);

        res.status(200).send(data);
        console.log("GET /weather Request Received!");
    }
    catch(err){
        console.error(err.message);
        res.status(400).json({error: err});
        console.log("GET /weather Request Failed!");
    }
});

module.exports = router;