const NodeCache = require('node-cache');
const myCache = new NodeCache();

module.exports = duration => (req, res, next) => {
    const key = '__express__' + req.originalUrl || req.url;
    const cachedBody = myCache.get(key);
    if (cachedBody) {
        res.send(cachedBody);
        return;
    } else {
        res.sendResponse = res.send;
        res.send = (body) => {
            myCache.set(key, body, duration);
            res.sendResponse(body);
        };
        next();
    }
};