const express = require("express");
const bodyParser = require("body-parser");
const route = require("./routes/Routes");

const app = express();

// CORS Middleware
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    next();
});

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

app.use("/api", route);

app.use(express.static("public"));

app.get("/api", function (req, res) {
    const date = new Date();
    res.json({
        currentDate: date.toDateString()
    });
    console.log("GET / Request Received!");
});

const port = process.env.PORT || 3500;
app.listen(port, function () {
    console.log(`Web Server Started on PORT: ${port}`);
});